class PersonalCard extends HTMLElement {
    constructor() {
        super();
    }

    crearElemento(nameElement) {
        let elemento = document.createElement(nameElement);
        return elemento;
    }

    connectedCallback() {
        const shadow = this.attachShadow({mode: 'open'});
        this.parrafoNombre = shadow.appendChild(this.crearElemento('span'));
        this.parrafoApellido = shadow.appendChild(this.crearElemento('span'));
        this.parrafoMail = shadow.appendChild(this.crearElemento('p'));
        this.parrafoSkynet = shadow.appendChild(this.crearElemento('p'));
        this.parrafoNombre.textContent = 'Mi nombre es ' + this.getAttribute('nombre') + ' '; 
        this.parrafoApellido.textContent = this.getAttribute('lastname'); 
        this.parrafoMail.textContent = 'Mi correo es ' + this.getAttribute('email');

        if (this.getAttribute('email').includes('gmail')) {
            this.parrafoSkynet.textContent = 'Eres parte de Skynet';
        } 
               
    }
    
    disconnectedCallback() {
       console.log('Removiendo NuevoElementoWeb');
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log('Cambian propiedades', name, oldValue, newValue);

        switch (name) {
            case 'nombre':
                if (newValue === 'Sandra') {
                    this.parrafoNombre.textContent  = 'Soy yo';
                } else {
                    this.parrafoNombre.textContent  = `Tu nombre es ${newValue} `;
                    this.parrafoApellido.textContent = ' '
                }
            break;
            case 'lastname':
                if (newValue === 'Díaz') {
                    this.parrafoApellido.textContent = ' '
                } else {
                    this.parrafoApellido.textContent  = newValue;
                }
            break;
            case 'email':
                this.parrafoMail.textContent = `Tu correo es ${newValue}`;
                if (newValue.includes('gmail')) {
                    this.parrafoSkynet.textContent = 'Eres parte de Skynet';
                } else {
                    this.parrafoSkynet.textContent = ' ';
                }
            break;
        }

    }

    static get observedAttributes() { return ['nombre', 'lastname', 'email']; }

}

customElements.define('presentation-card-sedz', PersonalCard);